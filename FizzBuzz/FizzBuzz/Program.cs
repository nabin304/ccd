﻿using System;

namespace FizzBuzz
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Write(FizzBuzz(100));
            Console.Read();
        }

        public static string FizzBuzz(int num)
        {
            string fizzbuzz = null;
            for (int i = 1; i <= num; i++)
            {
                fizzbuzz += CheckMultiple(i);
            }
            return fizzbuzz;
        }

        private static string CheckMultiple(int i)
        {
            string fizzbuzz = null;
            if (!CheckMultipleOfThree(i) && !CheckMultipleOfFive(i) && !CheckMultipleOfThreeAndFive(i))
                fizzbuzz = $"{i}\n\r";
            if (CheckMultipleOfThree(i))
                fizzbuzz = "Fizz\n\r";
            if (CheckMultipleOfFive(i))
                fizzbuzz = "Buzz\n\r";
            if (CheckMultipleOfThreeAndFive(i))
                fizzbuzz = "FizzBuzz\n\r";
            return fizzbuzz;
        }

        public static bool CheckMultipleOfThree(int num)
        {
            if (num % 3 == 0)
                return true;
            return false;
        }

        public static bool CheckMultipleOfFive(int num)
        {
            if (num % 5 == 0)
                return true;
            return false;
        }

        public static bool CheckMultipleOfThreeAndFive(int num)
        {
            if (num % 3 == 0 && num % 5 == 0)
                return true;
            return false;
        }
    }
}
