﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SC.Common.Exceptions
{
    public class SCException:ModuleException
    {
        public SCException(string message, Exception innerException ,params object[] args)
            :base(args.Length > 0
                ? string.Format(message, args)
                : message,innerException,CommonUtility.ParseErrorCode(message))
        {
        }
    }
}
