﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SC.Common.Exceptions
{
    public class BusinessLogicException : SCException
    {
        public BusinessLogicException(string message, Exception innerException, params object[] args)
            : base(message, innerException, args)
        {
        }
    }
}