﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SC.Common.Exceptions
{
    public class CommonException:Exception
    {
        public CommonException(int errorCode, string message, Exception innerException)
            : base(message, innerException)
        {
            ErrorCode = errorCode;
            Arguments = new string[0];
        }

        public ExceptionType ExceptionType { get; protected set; }
       
        public int ErrorCode { get; protected set; }

        public string[] Arguments { get; protected set; }
    }

    public enum ExceptionType
    {
        Unexpected,
        Error,
        Warning
    }
}
