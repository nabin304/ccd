﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SC.Common.Exceptions
{
    public class ModuleException:CommonException
    {
        public ModuleException(string message, Exception innerException, int errorCode)
            : base(errorCode, message, innerException)
        {
           ExceptionType = ExceptionType.Warning;
        }
    }
}
