﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SC.Common
{
    public class CommonUtility
    {
        public static int ParseErrorCode(string message)
        {
            int errorCode = -42;
            if (!String.IsNullOrEmpty(message))
            {
                var expression = new Regex("\\[(?<errorCode>\\d+)\\].*\\Z", RegexOptions.None);
                Match match = expression.Match(message);
                if (match.Success)
                {
                    Group errorCodeGroup = match.Groups["errorCode"];
                    string value = errorCodeGroup.Value;
                    Int32.TryParse(value, out errorCode);
                }
            }
            return errorCode;
        }
    }
}
