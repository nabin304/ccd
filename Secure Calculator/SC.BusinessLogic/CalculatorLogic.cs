﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SC.Common.Exceptions;
using SC.Resources;


namespace SC.BusinessLogic
{
    /// <summary>
    /// Main calculator class to perform calculator operations
    /// </summary>
    public class CalculatorLogic
    {
        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double Add(double x, double y)
        {
            return x + y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double Subtract(double x, double y)
        {
            return x - y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double Multiply(double x, double y)
        {
            return x*y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double Divide(double x, double y)
        {
            if (y == 0)
                throw new BusinessLogicException(ErrorResources.Err_1000, null);
            return x/y;
        }

        #endregion
    }
}