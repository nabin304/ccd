﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SC.Common.Exceptions;
using SC.Resources;

namespace SC.BusinessLogic
{
    public class ExceptionFactory
    {
        public static BusinessLogicException InvalidOpeartion(string opeartion, Exception innerException)
        {
            return new BusinessLogicException(ErrorResources.Err_1000, innerException,opeartion);
        }
    }
}
