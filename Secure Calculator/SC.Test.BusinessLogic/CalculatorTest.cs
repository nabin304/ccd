﻿using System;
using System.Security.Authentication;
using SC.BusinessLogic;
using SC.Common.Exceptions;
using Xunit;

namespace SC.Test.BusinessLogic
{
    public class CalculatorTest
    {
        [Fact]
        public void TestDivide()
        {
            CalculatorLogic calculatorLogic = new CalculatorLogic();
            var exception = Assert.Throws<BusinessLogicException>(() => calculatorLogic.Divide(1, 0));
            Assert.Equal(1000, exception.ErrorCode);
            Assert.Equal(2,calculatorLogic.Divide(50,25));
        }

        [Fact]
        public void TestAdd()
        {
            CalculatorLogic calculatorLogic = new CalculatorLogic();
            Assert.Equal(75, calculatorLogic.Add(50, 25));
        }

        [Fact]
        public void TestSubtract()
        {
            CalculatorLogic calculatorLogic = new CalculatorLogic();
            Assert.Equal(25, calculatorLogic.Subtract(50, 25));
        }

        [Fact]
        public void TestMultiply()
        {
            CalculatorLogic calculatorLogic = new CalculatorLogic();
            Assert.Equal(150, calculatorLogic.Multiply(50, 3));
        }
    }
}