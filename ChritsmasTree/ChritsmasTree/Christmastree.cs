﻿using System;

namespace tree
{
    public static class Christmastree
    {
        /// <summary>
        /// Draw christmas tree
        /// </summary>
        /// <param name="line"></param>
        /// <param name="hasTop"></param>
        /// <returns></returns>
        public static string Draw(int line, bool hasTop = false)
        {
            if(line<1)
                throw new ArgumentException("Line number must be positive");
            return hasTop ? DrawTop(line) + DrawBody(line) + DrawRoot(line) : DrawBody(line) + DrawRoot(line);
        }

        private static string DrawTop(int line)
        {
            string top = null;
            for (int i = 0; i < line - 1; i++)
                top += " ";
            return top += "*\n\r";
        }

        private static string DrawBody(int line)
        {
            string tree = null;
            for (int currentLine = 1; currentLine <= line; currentLine++)
                tree += InsertSpace(line, currentLine) + DrawCharacter(currentLine) + "\n\r";
            return tree;
        }

        private static string DrawRoot(int line)
        {
            string root = null;
            for (int i = 0; i < line - 1; i++)
                root += " ";
            return root += "x";
        }

        private static string DrawCharacter(int currentLine)
        {
            string drawCharacter = null;
            for (int j = 0; j < 2 * currentLine - 1; j++)
                drawCharacter += "x";
            return drawCharacter;
        }

        private static string InsertSpace(int line, int currentLine)
        {
            string insertSpace = null;
            for (int j = 1; j <= line - currentLine; j++)
                insertSpace += " ";
            return insertSpace;
        }
    }
}