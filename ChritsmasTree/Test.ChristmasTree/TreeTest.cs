﻿using System;
using tree;
using Xunit;

namespace ChristmasTreeTest
{
    public class TreeTest
    {
        [Fact]
        public void TestDrawTreeWithOutTop()
        {
            string expectedResult = "  x\n\r xxx\n\rxxxxx\n\r  x";
            Assert.Equal(expectedResult, Christmastree.Draw(3));
        }

        [Fact]
        public void TestDrawTreeWithTop()
        {
            string expectedResult = "  *\n\r  x\n\r xxx\n\rxxxxx\n\r  x";
            Assert.Equal(expectedResult, Christmastree.Draw(3, true));
        }

        [Fact]
        public void TestDrawTreeWithNegativeLineNumber()
        {
            var exception= Assert.Throws<ArgumentException>(() => Christmastree.Draw(-5));
            Assert.Equal("Line number must be positive",exception.Message);

        }
  

    #region Private Method Test

    /*
            [Fact]
            public void TestDrawTop()
            {
                string expectedResult = "  *\n\r";
                Assert.Equal(expectedResult, Christmastree.DrawTop(3));
            }
    
            [Fact]
            public void TestInsertSpace()
            {
                string expectedResult = "   ";
                Assert.Equal(expectedResult, Christmastree.InsertSpace(4, 1));
            }
    
            [Fact]
            public void TestDrawCharacter()
            {
                string expectedResult = "xxxxx";
                Assert.Equal(expectedResult, Christmastree.DrawCharacter(3));
            }
    
            [Fact]
            public void TestDrawBody()
            {
                string expectedResult = "  x\n\r xxx\n\rxxxxx\n\r";
                Assert.Equal(expectedResult, Christmastree.DrawBody(3));
            }
    
            [Fact]
            public void TestDrawRoot()
            {
                string expectedResult = "  x";
                Assert.Equal(expectedResult, Christmastree.DrawRoot(3));
            }*/

    #endregion
}

}